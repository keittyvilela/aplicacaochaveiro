using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace App4.Models
{
    class Cliente
    {
        [JsonProperty("ClienteCpf")]
        public string ClienteCpf { get; set; }
        [JsonProperty("ClienteNome")]
        public string ClienteNome { get; set; }
        [JsonProperty("ClienteEmail")]
        public string ClienteEmail { get; set; }
        [JsonProperty("ClienteSenha")]
        public string ClienteSenha { get; set; }
        [JsonProperty("ClienteCartao")]
        public string ClienteCartao { get; set; }
        [JsonProperty("ClienteDeviceId")]
        public string ClienteDeviceId { get; set; }
        [JsonProperty("ClienteHardwareId")]
        public string ClienteHardwareId { get; set; }


    }
}