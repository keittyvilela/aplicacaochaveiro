﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace App4.Models
{

    class SolicitacaoAtendimento
    {
        [JsonProperty("Id")]
        public string IdUsuario { get; set; }
        [JsonProperty("Lat")]
        public string GeoLatitude { get; set; }
        [JsonProperty("Longi")]
        public string GeoLongitude { get; set; }
        [JsonProperty("Servico")]
        public List<ListaSevicos> Servicos { get; set; }





    }
}