﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App4.Models
{
    class PedidoCliente

    {
        public Cliente Cliente { get; set; }
        public Servico Servico { get; set; }
        public long Longitude { get; set; }
        public long Latitude { get; set; }

    }
}