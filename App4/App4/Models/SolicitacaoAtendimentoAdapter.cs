﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App4.Models
{

    //CLASSE HOLDER DO LIST VIEW
    public class ViewHolder1 : Java.Lang.Object
    {
        public TextView txtName { get; set; }
    }


    class SolicitacaoAtendimentoAdapter : BaseAdapter
    {
        private Activity activity;
        private List<ListaAtendimentos> atend;


        //CONSTRUTOR
        public SolicitacaoAtendimentoAdapter(Activity activity, List<ListaAtendimentos> atend)
        {
            this.activity = activity;
            this.atend = atend;
        }



        public override int Count
        {
            get
            {
                return atend.Count;
            }
        }



        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }


        //BUSCAR O ITEM POR ID (JSON TRAZ TODOS COMO STRING)
        public override long GetItemId(int position)
        {
            int v;
            //ANTES DE RETORNAR CONVERTE EM INT
            return v = Convert.ToInt32(atend[position].Atendimento.IdUsuario);

        }



        //METODO PARA MOSTRAR INFORMAÇÕES NO LISTVIEW
        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            //LINHA PRA CHAMAR O TEMPLATE USADO - listaTemplate - CRIADO NO LAYOUT
            var view = convertView ?? activity.LayoutInflater.Inflate(Resource.Layout.TemplateListaAtendimento, parent, false);

            //CHAMAR OS CAMPOS DO TEMPLATE
            var txtName = view.FindViewById<TextView>(Resource.Id.textView1);
            var txtValor = view.FindViewById<TextView>(Resource.Id.textView2);
            var txtServ = view.FindViewById<TextView>(Resource.Id.textView3);

            //COLOCAR OS VALORES DOS SERVICOS CRIADOS NO JSON
            txtName.Text = atend[position].Atendimento.IdUsuario;
            //txtValor.Text = atend[position].Atendimento.Servicos[position].Servico.Nome;

            txtServ.Text = atend[position].Atendimento.Servicos[0].Servico.Nome;
            txtValor.Text = atend[position].Atendimento.Servicos[0].Servico.Valor;

            //RETORNA A VIEW DO TEMPLATE
            return view;

        }
    }
}