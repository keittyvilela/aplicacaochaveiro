using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;


namespace App4.Models
{
    [Serializable]
    class Servico
    {
        [JsonProperty("Id")]
        public string Id { get; set; }
        [JsonProperty("Nome")]
        public string Nome { get; set; }
        [JsonProperty("Valor")]
        public string Valor { get; set; }


    }
}