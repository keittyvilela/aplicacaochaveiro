﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace App4.Models
{
    class Chaveiro
    {
        [JsonProperty("ChaveiroNome")]
        public string ChaveiroNome { get; set; }
        [JsonProperty("ChaveiroEmail")]
        public string ChaveiroEmail { get; set; }
        [JsonProperty("ChaveiroSenha")]
        public string ChaveiroSenha { get; set; }
        [JsonProperty("ChaveiroPaypal")]
        public string ChaveiroPaypal { get; set; }
        [JsonProperty("ChaveiroDeviceId")]
        public string ChaveiroDeviceId { get; set; }
        [JsonProperty("ChaveiroHardwareId")]
        public string ChaveiroHardwareId { get; set; }
        [JsonProperty("ChaveiroCNPJ")]
        public string ChaveiroCNPJ { get; set; }
        [JsonProperty("ChaveiroRazaoSocial")]
        public string ChaveiroRazaoSocial { get; set; }
        [JsonProperty("ChaveiroNomeFantasia")]
        public string ChaveiroNomeFantasia { get; set; }
        [JsonProperty("Telefone")]
        public string Telefone { get; set; }
        [JsonProperty("RaioAtuacao")]
        public string RaioAtuacao { get; set; }
        [JsonProperty("ChaveiroGeoLatitude")]
        public string ChaveiroGeoLatitude { get; set; }
        [JsonProperty("ChaveiroGeoLongitude")]
        public string ChaveiroGeoLongitude { get; set; }
        [JsonProperty("ChaveiroDocumentos")]
        public List<Documento> ChaveiroDocumentos { get; set; }
        [JsonProperty("ChaveiroId")]
        public string ChaveiroId { get; set; }




}
}