﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace App4.Models
{
    class Usuario
    {
        [JsonProperty("Id")]
        public string Id { get; set; }
        [JsonProperty("Email")]
        public string Email { get; set; }
        [JsonProperty("Senha")]
        public string Senha { get; set; }
        [JsonProperty("Situacao")]
        public string Situacao { get; set; }
        [JsonProperty("Tipo")]
        public string Tipo { get; set; }

    }
}