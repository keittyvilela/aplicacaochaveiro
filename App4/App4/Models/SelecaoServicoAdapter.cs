﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using App4.Models;
using Android;

namespace App4.Models
{
    //CLASSE HOLDER DO LIST VIEW
    public class ViewHolder : Java.Lang.Object
    {
        public TextView txtName { get; set; }
    }


     class SelecaoServicoAdapter : BaseAdapter
    {
        private Activity activity;
        private List<ListaSevicos> serv;


        //CONSTRUTOR
        public SelecaoServicoAdapter(Activity activity, List<ListaSevicos> serv)
        {
            this.activity = activity;
            this.serv = serv;
        }

      
        
        public override int Count
        {
            get
            {
                return serv.Count;
            }
        }



        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }


        //BUSCAR O ITEM POR ID (JSON TRAZ TODOS COMO STRING)
        public override long GetItemId(int position)
        {
            int v;
            //ANTES DE RETORNAR CONVERTE EM INT
            return v = Convert.ToInt32(serv[position].Servico.Id);
      
        }



        //METODO PARA MOSTRAR INFORMAÇÕES NO LISTVIEW
        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            //LINHA PRA CHAMAR O TEMPLATE USADO - listaTemplate - CRIADO NO LAYOUT
            var view = convertView ?? activity.LayoutInflater.Inflate(Resource.Layout.TemplateListaServicos, parent, false);

            //CHAMAR OS CAMPOS DO TEMPLATE
            var txtName = view.FindViewById<TextView>(Resource.Id.textView1);
            var txtValor = view.FindViewById<TextView>(Resource.Id.textView2);
            //COLOCAR OS VALORES DOS SERVICOS CRIADOS NO JSON
            txtName.Text = serv[position].Servico.Nome;
            txtValor.Text = serv[position].Servico.Valor;
            //RETORNA A VIEW DO TEMPLATE
            return view;

        }
    }
}