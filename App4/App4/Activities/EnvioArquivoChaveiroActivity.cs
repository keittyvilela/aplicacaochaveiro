using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App4.Activities
{
    [Activity(Label = "EnvioArquivoChaveiroActivity")]
    public class EnvioArquivoChaveiroActivity : Activity
    {

        Button buttonEnviarArquivo;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.EnvioArquivoChaveiro);

            buttonEnviarArquivo = FindViewById<Button>(Resource.Id.enviaArquivosButton);

            buttonEnviarArquivo.Click += delegate {

                StartActivity(typeof(SelecaoServicosActivity));
            };

        }
    }
}