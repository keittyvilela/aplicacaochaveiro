﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App4.Models;
using System.Net;
using Newtonsoft.Json;

namespace App4.Activities
{
    [Activity(Label = "EsperaChaveiroActivity")]
    public class EsperaChaveiroActivity : Activity
    {

        //URL DO SERVICO GET
        //string url = "https://juansilveira.outsystemscloud.com/UnlockServices/rest/Listas/GetListaServicos";

        //LISTA DE SERVICO PARA RECEBIMENTO DOS VALORES DO JSON
        List<ListaAtendimentos> listaAtendimento;
        SolicitacaoAtendimento solAtendimento = new SolicitacaoAtendimento();

        //REFERENCIA DO PEDIDO DO CLIENTE 
        static PedidoCliente pedido = new PedidoCliente();


        //METODO ONCREATE
        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.SelecaoServicos);

            //CAHAMADA DO MÉTODO PARA LISTAR OS SERVICOS
            listaServicoGET();
        }



        //METODO PARA PEGAR VALORES JSON DOS SERVICOS
        public void listaServicoGET()
        {

            /*
            //ABERTURA DE COMUNICAÇÃO ARQUIVO JSON
            var request = HttpWebRequest.Create(url);
            request.ContentType = "application/json";
            request.Method = "GET";


            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Console.Out.WriteLine("ERRO STATUS CODE : {0}", response.StatusCode);
                }
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    string content = reader.ReadToEnd();

                    if (string.IsNullOrWhiteSpace(content))
                    {
                        Console.Out.WriteLine("SEM CONTEUDO!! ");
                    }
                    else
                    {
                        //LINHA PARA RECEBER OS VALORES DO ARQUIVO JSON DESERIALIZADOS DENTRO DA LISTA DE SERVICOS
                        listaServicos = JsonConvert.DeserializeObject<List<ListaSevicos>>(content);
                    }
                }
            }
            */

            string content = "[{\"SolicitacaoAtendimento\":{\"Id\":\"Cliente teste 1\",\"Lat\":\"-25.4284\",\"Longi\":\"-49.2733\",\"Servico\":[{\"Id\":1,\"Nome\":\"Servco 01\", \"Valor\":50}]}}]";
            listaAtendimento = JsonConvert.DeserializeObject<List<ListaAtendimentos>>(content);


            //CHAMADA DA CLASSE ADAPTER PARA MONTAR A EXIBIÇÃO DO LISTVIEW
            var lstData = FindViewById<ListView>(Android.Resource.Id.List);
            var adapter = new SolicitacaoAtendimentoAdapter(this, listaAtendimento);
            lstData.Adapter = adapter;

        } //FIM METODO LISTAGET


        /*
        //METODO PARA OUVIR O CLICK NO ITEM
        protected override void OnListItemClick(ListView l, View v, int position, long id)

        {

            var t = listaAtendimento[position].Atendimento.IdUsuario;
            //MOSTRA MENSAGEM DE QUAL SERVICO FOI SELECIONADO
            Android.Widget.Toast.MakeText(this, t + " selecionado!", Android.Widget.ToastLength.Short).Show();



        }*/


    }

}