using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App4.Models;
using Java.Util;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.IO;
using Newtonsoft.Json;
using Android.Locations;
using Android.Util;
using Firebase.Iid;
using Android.Gms.Common;

namespace App4.Activities
{
    [Activity(Label = "SelecaoServicosActivity")]
    public class SelecaoServicosActivity : ListActivity, ILocationListener
    {

        //URL DO SERVICO GET
        string url = "https://juansilveira.outsystemscloud.com/UnlockServices/rest/Listas/GetListaServicos";

        Atendimento atendimento = new Atendimento();
        //LISTA DE SERVICO PARA RECEBIMENTO DOS VALORES DO JSON
        List<ListaSevicos> listaServicos;
        Servico servicoSelecionado = new Servico();

        //Localizacao
        string tag = "SelecaoServicosActivity";
        LocationManager locMgr;

        //HASH GOOGLE
        const string TAG = "SelecaoServicosActivity";

        //METODO ONCREATE
        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.SelecaoServicos);

            //HASH GOOGLE
            if (Intent.Extras != null)
            {
                foreach (var key in Intent.Extras.KeySet())
                {
                    var value = Intent.Extras.GetString(key);
                    Android.Util.Log.Debug(TAG, "Key: {0} Value: {1}", key, value);
                }
            }

            IsPlayServicesAvailable();



            // initialize location manager
            locMgr = GetSystemService(Context.LocationService) as LocationManager;


            //CAHAMADA DO M�TODO PARA LISTAR OS SERVICOS
            listaServicoGET();
        }



        //METODO PARA PEGAR VALORES JSON DOS SERVICOS
        public void listaServicoGET()
        {

            //ABERTURA DE COMUNICA��O ARQUIVO JSON
            var request = HttpWebRequest.Create(url);
            request.ContentType = "application/json";
            request.Method = "GET";


            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Console.Out.WriteLine("ERRO STATUS CODE : {0}", response.StatusCode);
                }
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    string content = reader.ReadToEnd();

                    if (string.IsNullOrWhiteSpace(content))
                    {
                        Console.Out.WriteLine("SEM CONTEUDO!! ");
                    }
                    else
                    {
                        //LINHA PARA RECEBER OS VALORES DO ARQUIVO JSON DESERIALIZADOS DENTRO DA LISTA DE SERVICOS
                        listaServicos = JsonConvert.DeserializeObject<List<ListaSevicos>>(content);
                    }
                }
            }

            //CHAMADA DA CLASSE ADAPTER PARA MONTAR A EXIBI��O DO LISTVIEW
            var lstData = FindViewById<ListView>(Android.Resource.Id.List);
            var adapter = new SelecaoServicoAdapter(this, listaServicos);
            lstData.Adapter = adapter;

        } //FIM METODO LISTAGET



        //METODO PARA OUVIR O CLICK NO ITEM
        protected override void OnListItemClick(ListView l, View v, int position, long id)

        {

            var t = listaServicos[position].Servico.Nome;
            //MOSTRA MENSAGEM DE QUAL SERVICO FOI SELECIONADO
            Android.Widget.Toast.MakeText(this, t + " selecionado!", Android.Widget.ToastLength.Short).Show();
            //PEGA SERVICO
            servicoSelecionado = listaServicos[position].Servico;



            //PEGA LOCALIZACAO
            if (locMgr.AllProviders.Contains(LocationManager.NetworkProvider)
                   && locMgr.IsProviderEnabled(LocationManager.NetworkProvider))
            {
                //provider
                //minimo de secundos para atualizar
                //quatidade de metros para acusar deslocamento de localiza��o
                locMgr.RequestLocationUpdates(LocationManager.NetworkProvider, 2000, 5, this);
                
            }
            else
            {
                Toast.MakeText(this, "The Network Provider does not exist or is not enabled!", ToastLength.Long).Show();
            }



            //PEGO HASH

            if (!GetString(Resource.String.google_app_id).Equals("1:693051587992:android:ca28c4acf068f299"))
                throw new System.Exception("Invalid Json file");

            Task.Run(() => {

                var instanceId = FirebaseInstanceId.Instance;
                instanceId.DeleteInstanceId();
                Android.Util.Log.Debug("TAG", "{0} {1}", instanceId.Token, instanceId.GetToken(GetString(Resource.String.gcm_defaultSenderId), Firebase.Messaging.FirebaseMessaging.InstanceIdScope));


                atendimento.UserHash = instanceId.GetToken(GetString(Resource.String.gcm_defaultSenderId), Firebase.Messaging.FirebaseMessaging.InstanceIdScope);
               
            });

            /*
            //PASSANDO OS SERVICOS PARA ACTIVITY LOCALIZACAO E CHAMANDO ELA
            var activity = new Intent(this, typeof(LocalizacaoActivity));
            activity.PutExtra("servicoSelecionado", (Java.IO.ISerializable)servicoSelecionado);
            StartActivity(activity); */

           
            var idSer = listaServicos[position].Servico.Id;
            atendimento.ServicoId = idSer;
            var idVal = listaServicos[position].Servico.Valor;
            atendimento.Valor = idVal;
            atendimento.GeoLatitude = "-25.4977697";
            atendimento.GeoLongitude = "-49.3123075";

            MandarJsonAtendimento(atendimento);
            StartActivity(typeof(EsperaClienteActivity));
            
        }

        async Task<string> MandarJsonAtendimento(Atendimento atend)
        {
            string url1 = "https://juansilveira.outsystemscloud.com/UnlockServices/rest/Atendimento/SolicitarAtendimento";
            string json = JsonConvert.SerializeObject(atend);

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url1));
            request.ContentType = "application/json";
            request.Method = "POST";
            request.ContentLength = json.Length;

            try
            {
                using (WebResponse response = await request.GetResponseAsync())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            string resposta = reader.ReadToEnd();

                            return resposta;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        




        public bool IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);

            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))

                    Android.Widget.Toast.MakeText(this, GoogleApiAvailability.Instance.GetErrorString(resultCode), Android.Widget.ToastLength.Short).Show();

                else
                {
                    Android.Widget.Toast.MakeText(this, "This device is not supported", Android.Widget.ToastLength.Short).Show();
                    Finish();
                }
                return false;
            }
            else
            {
                Android.Widget.Toast.MakeText(this, "Google Play Services is available.", Android.Widget.ToastLength.Short).Show();
                return true;
            }
        }




        //METODOS CLASSE IMPKLEMENTADA ILOCATIONS
        public void OnLocationChanged(Location location)
        {
            Log.Debug(tag, "A sua localiza��o mudou!");

            location.Latitude.ToString();
            location.Longitude.ToString();
        }

        public void OnProviderDisabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnProviderEnabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
        {
            throw new NotImplementedException();
        }
    }

}