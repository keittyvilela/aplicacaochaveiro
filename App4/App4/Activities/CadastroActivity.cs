using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App4.Models;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Net.Http;

namespace App4.Activities
{
    [Activity(Label = "CadastroActivity")]
    public class CadastroActivity : Activity

    {

        Button buttonCadastrar;
        EditText cpf;
        EditText nome;
        EditText email;
        EditText cartao;
        EditText senha;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);


            SetContentView(Resource.Layout.Cadastro);

            //Bot�o para cadastrar um novo cliente
            buttonCadastrar = FindViewById<Button>(Resource.Id.cadastrarButton);
            //Campos da tela cadastrar cliente
            cpf = FindViewById<EditText>(Resource.Id.cpfEditText);
            nome = FindViewById<EditText>(Resource.Id.nomeCompletoEditText);
            email = FindViewById<EditText>(Resource.Id.emailEditText);
            cartao = FindViewById<EditText>(Resource.Id.cartaoEditText);
            senha = FindViewById<EditText>(Resource.Id.senhaEditText);



            buttonCadastrar.Click += delegate
            {

                Task<string> result = CadastrarCliente(cpf.Text, nome.Text, email.Text, cartao.Text, senha.Text); //metodo para cadastrar um novo cliente
                                                    
                Android.Widget.Toast.MakeText(this, "Cadastro efetuado!", Android.Widget.ToastLength.Short).Show();

                StartActivity(typeof(SelecaoServicosActivity));
            };
        }



        //Metodo cadastrar cliente
        public async Task<string> CadastrarCliente(string cpf1, string nome1, string email1, string cartao, string senha1)
        {
            string url = "https://juansilveira.outsystemscloud.com/UnlockServices/rest/Cadastros/CadastrarCliente";


            Cliente cliente = new Cliente();
            cliente.ClienteCpf = cpf1.ToString();
            cliente.ClienteNome = nome1.ToString();
            cliente.ClienteEmail = email1.ToString();
            cliente.ClienteSenha = senha1.ToString();
            cliente.ClienteCartao = cartao.ToString();
            cliente.ClienteDeviceId = Android.OS.Build.Serial;
            cliente.ClienteHardwareId = Android.OS.Build.Hardware;



            string json = JsonConvert.SerializeObject(cliente);

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
            request.ContentType = "application/json";
            request.Method = "POST";
            request.ContentLength = json.Length;
                        
            try
            {
                using (WebResponse response = await request.GetResponseAsync())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            string resposta = reader.ReadToEnd();

                            return resposta;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                return ex.Message;
            }
        }


    }
}