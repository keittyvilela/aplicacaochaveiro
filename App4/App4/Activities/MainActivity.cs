﻿using Android.App;
using Android.Widget;
using Android.OS;
using App4.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using Android.Content;
using Android.Gms.Common;
using Firebase.Iid;
using Android.Util;
using System.Threading.Tasks;

namespace App4.Activities
{


    [Activity(Label = "App4", MainLauncher = true, Icon = "@drawable/icon")]

  

    public class MainActivity : Activity
    {

        List<ListaUsuarios> listaUsuarios;

        Button buttonCadastrarNovoCliente;
        Button buttonLogin;
        Button buttonCadastrarNovoChaveiro;

        EditText usuarioEmail;
        EditText usuarioSenha;

        int tipoUsuario = 1;
        const string TAG = "MainActivity";


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);


            buttonCadastrarNovoCliente = FindViewById<Button>(Resource.Id.cadastrarNovoButton);
            buttonLogin = FindViewById<Button>(Resource.Id.loginButton);
            buttonCadastrarNovoChaveiro = FindViewById<Button>(Resource.Id.cadastrarNovoChavButton);


            /*
            if (Intent.Extras != null)
            {
                foreach (var key in Intent.Extras.KeySet())
                {
                    var value = Intent.Extras.GetString(key);
                    Android.Util.Log.Debug(TAG, "Key: {0} Value: {1}", key, value);
                }
            }

            IsPlayServicesAvailable();*/

            buttonCadastrarNovoCliente.Click += delegate {

                StartActivity(typeof(CadastroActivity));
                
            };

            buttonLogin.Click += delegate {

                /*
                Log.Debug(TAG, "InstanceID token: " + FirebaseInstanceId.Instance.Token);
                Firebase.Messaging.FirebaseMessaging.Instance.SubscribeToTopic("news");
                Android.Util.Log.Debug(TAG, "Subscribed to remote notifications");
                */

                /*
                if (!GetString(Resource.String.google_app_id).Equals("1:693051587992:android:ca28c4acf068f299"))
                    throw new System.Exception("Invalid Json file");

                Task.Run(() => {

                    var instanceId = FirebaseInstanceId.Instance;
                    instanceId.DeleteInstanceId();
                    Android.Util.Log.Debug("TAG", "{0} {1}", instanceId.Token, instanceId.GetToken(GetString(Resource.String.gcm_defaultSenderId), Firebase.Messaging.FirebaseMessaging.InstanceIdScope));

                }); */


                usuarioEmail = FindViewById<EditText>(Resource.Id.loginEditText);
                usuarioSenha = FindViewById<EditText>(Resource.Id.senhaEditText);

                tipoUsuario = RealizaLogin(usuarioEmail.Text, usuarioSenha.Text);

                //se o tipo do usuario for 1, cliente 
                //se o tipo do usuario for 2, chaveiro
                //se o retorno do metodo for 0 não existe usuario cadastrado
                //se o retorno do metodo for 4 a senha esta incorreta

                if (tipoUsuario == 1)
                {
                    Android.Widget.Toast.MakeText(this, "Login Efetuado!", Android.Widget.ToastLength.Short).Show();
                    StartActivity(typeof(SelecaoServicosActivity));

                }
                else if (tipoUsuario == 2)
                {
                    Android.Widget.Toast.MakeText(this, "Login Efetuado!", Android.Widget.ToastLength.Short).Show();
                    StartActivity(typeof(EsperaChaveiroActivity));
                }
                else if (tipoUsuario == 4)
                {
                    Android.Widget.Toast.MakeText(this, "Senha incorreta!", Android.Widget.ToastLength.Short).Show();
                }
                else
                {
                    Android.Widget.Toast.MakeText(this, "Usuário não encontrado!", Android.Widget.ToastLength.Short).Show();
                }
            };

            buttonCadastrarNovoChaveiro.Click += delegate {

                StartActivity(typeof(CadastroChaveiroActivity));
            };


            

        }

        /*
        public bool IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);

            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))

                    Android.Widget.Toast.MakeText(this, GoogleApiAvailability.Instance.GetErrorString(resultCode), Android.Widget.ToastLength.Short).Show();

                else
                {
                    Android.Widget.Toast.MakeText(this, "This device is not supported", Android.Widget.ToastLength.Short).Show();
                    Finish();
                }
                return false;
            }
            else
            {
                Android.Widget.Toast.MakeText(this, "Google Play Services is available.", Android.Widget.ToastLength.Short).Show();
                return true;
            }
        }
        */

        public int RealizaLogin(string email, string senha)
        {
            /*
            string url = "";
            
            var request = HttpWebRequest.Create(url);
            request.ContentType = "application/json";
            request.Method = "GET";
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Console.Out.WriteLine("ERRO STATUS CODE : {0}", response.StatusCode);
                }
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    string content = reader.ReadToEnd();

                    if (string.IsNullOrWhiteSpace(content))
                    {
                        Console.Out.WriteLine("SEM CONTEUDO!! ");
                    }
                    else
                    {
                       listaUsuarios = JsonConvert.DeserializeObject<List<ListaUsuarios>>(content);
                    }
                }
            }
             */

            string content = "[{\"Usuario\":{\"Id\":1,\"Email\":\"keitty@gmail.com\",\"Senha\":123, \"Situacao\":1, \"Tipo\":1}}, {\"Usuario\":{\"Id\":2, \"Email\":\"juan@gmail.com\",\"Senha\":123, \"Situacao\":1, \"Tipo\":2}}]";
            listaUsuarios = JsonConvert.DeserializeObject<List<ListaUsuarios>>(content);

            foreach (ListaUsuarios lista in listaUsuarios)
            {
                if (email.Equals(lista.Usuario.Email))
                {
                    if (senha.Equals(lista.Usuario.Senha))
                    {
                        int tipo = Int32.Parse(lista.Usuario.Tipo);

                        return tipo;
                    }
                    else
                    {
                        return 4; //se retorna 4 a senha incorreta
                    }
                }
            }
            return 0; //se retorna zero não existe
        }
    }



}

