﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;
using Android.Util;
using App4.Models;

namespace App4.Activities
{
    [Activity(Label = "LocalizacaoActivity")]
    public class LocalizacaoActivity : Activity, ILocationListener
    {
        Button buttonLocalizacao;
        LocationManager locMgr;
        Servico s;
        string tag = "LocalizacaoActivity";


        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Localizacao);

            
            //Retorno na activity anterior de serviicos - SelecaoServicosActivity
            s = (Servico)Intent.GetSerializableExtra("servicoSelecionado"); 

            
            //Retorno na activity anterior de serviicos - SelecaoServicosActivity
            //string text = Intent.GetStringExtra("pedido") ?? "Data not available";

            //Botão para cadastrar um novo cliente
            buttonLocalizacao = FindViewById<Button>(Resource.Id.buscarLocalizacao);

            // initialize location manager
            locMgr = GetSystemService(Context.LocationService) as LocationManager;

            //CLICK DO BOTÃO
            buttonLocalizacao.Click += delegate {

                if (locMgr.AllProviders.Contains(LocationManager.NetworkProvider)
                    && locMgr.IsProviderEnabled(LocationManager.NetworkProvider))
                {
                    //provider
                    //minimo de secundos para atualizar
                    //quatidade de metros para acusar deslocamento de localização
                    locMgr.RequestLocationUpdates(LocationManager.NetworkProvider, 2000, 5, this);


                    StartActivity(typeof(EsperaClienteActivity));
                }
                else
                {
                    Toast.MakeText(this, "The Network Provider does not exist or is not enabled!", ToastLength.Long).Show();
                }
            };
        }


        public void OnLocationChanged(Location location)
        {
            Log.Debug(tag, "A sua localização mudou!");

            location.Latitude.ToString();
            location.Longitude.ToString();

        }

        public void OnProviderDisabled(string provider)
        {
            Log.Debug(tag, provider + " desabilitador pelo usuário!");
        }

        public void OnProviderEnabled(string provider)
        {
            Log.Debug(tag, provider + " habilitado pelo usuário");
        }

        public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
        {
            Log.Debug(tag, provider + " disponibilidade alterar para " + status.ToString());
        }


    }
}