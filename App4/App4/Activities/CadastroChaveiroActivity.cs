using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using App4.Models;
using Newtonsoft.Json;
using System.Net;
using System.IO;

using System.Net.Http;


namespace App4.Activities
{
    [Activity(Label = "CadastroChaveiroActivity")]
    public class CadastroChaveiroActivity : Activity
    {
        Button cadastrarButtonChav;
        EditText ChaveiroCnpj;
        EditText ChaveiroRazao;
        EditText ChaveiroNomeCompleto;
        EditText ChaveiroTelefone;
        EditText ChaveiroEmail;
        EditText ChaveiroContaPaypal;
        



    protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.CadastroChaveiro);

            //Bot�o para cadastrar um novo cliente
            cadastrarButtonChav = FindViewById<Button>(Resource.Id.cadastrarButtonChav);
            ChaveiroCnpj = FindViewById<EditText>(Resource.Id.cnpjEditText);
            ChaveiroRazao = FindViewById<EditText>(Resource.Id.razaoSocialEditText);
            ChaveiroNomeCompleto = FindViewById<EditText>(Resource.Id.nomeCompletoEditText);
            ChaveiroTelefone = FindViewById<EditText>(Resource.Id.telefoneEditText);
            ChaveiroEmail = FindViewById<EditText>(Resource.Id.emailEditText);
            ChaveiroContaPaypal = FindViewById<EditText>(Resource.Id.contaPayPalInputLayoutEditText);




            cadastrarButtonChav.Click += delegate
            {
                string result = CadastrarChaveiro(ChaveiroCnpj.Text, ChaveiroRazao.Text, ChaveiroNomeCompleto.Text, ChaveiroTelefone.Text, ChaveiroEmail.Text, ChaveiroContaPaypal.Text); //metodo para cadastrar um novo cliente

                Android.Widget.Toast.MakeText(this, "Cadastro efetuado!", Android.Widget.ToastLength.Short).Show();

                StartActivity(typeof(SelecaoServicosActivity));;
            };
        }


        //Metodo cadastrar cliente
        public string CadastrarChaveiro(string ChaveiroCnpj, string ChaveiroRazao, string ChaveiroNomeCompleto, string ChaveiroTelefone, string ChaveiroEmail, string ChaveiroContaPaypal)
        {
            Chaveiro chaveiro = new Chaveiro();
            string url = "https://juansilveira.outsystemscloud.com/UnlockServices/rest/Cadastros/CadastrarChaveiro";
            
            List<Documento> lstDoc = new List<Documento>();
            
            chaveiro.ChaveiroCNPJ = ChaveiroCnpj.ToString();
            chaveiro.ChaveiroDeviceId = Android.OS.Build.Serial;
            chaveiro.ChaveiroDocumentos = lstDoc;
            chaveiro.ChaveiroEmail = ChaveiroEmail.ToString();
            chaveiro.ChaveiroGeoLatitude = "";
            chaveiro.ChaveiroGeoLongitude = "";
            chaveiro.ChaveiroHardwareId = Android.OS.Build.Hardware; 
            chaveiro.ChaveiroId = "";
            chaveiro.ChaveiroNome = ChaveiroNomeCompleto.ToString();
            chaveiro.ChaveiroNomeFantasia = ChaveiroNomeCompleto.ToString();
            chaveiro.ChaveiroPaypal = ChaveiroContaPaypal.ToString();
            chaveiro.ChaveiroRazaoSocial = ChaveiroRazao.ToString();
            chaveiro.ChaveiroSenha = "" ;
            

            string json = JsonConvert.SerializeObject(chaveiro);

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");
                    var result = client.PostAsync("", content).Result;
                    var resultContent = result.Content;
                    string response = resultContent.ReadAsStringAsync().Result;


                    return response;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}