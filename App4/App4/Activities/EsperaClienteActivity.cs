﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.IO;
using App4.Models;
using Newtonsoft.Json;

namespace App4.Activities
{
    [Activity(Label = "EsperaActivity")]
    public class EsperaClienteActivity : Activity
    {


        Button buttonCancelar;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.EsperaCliente);

            //Botão para cadastrar um novo cliente
            buttonCancelar = FindViewById<Button>(Resource.Id.cancelaratendimento);


            buttonCancelar.Click += delegate
            {
                Android.Widget.Toast.MakeText(this, "Atendimento Cancelado!", Android.Widget.ToastLength.Short).Show();



                StartActivity(typeof(SelecaoServicosActivity));
            };

        }


        public void CancelarAtendimento()
        {

        }


       

    }
}